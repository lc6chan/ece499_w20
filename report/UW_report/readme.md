One copy of a final unbound report must be submitted to the project supervisor 
one week before the last day of lectures of the
appropriate term for marking by the supervisor and a co-reader chosen by the supervisor. 

After final corrections, an unbound copy of the report must be delivered to the department 
where it will be bound and kept in the Department Office. 
All copies should be typed, double-spaced, and single-sided on 81⁄2 × 11 inch bond paper.

The final report must contain at least the following 
    a) a dated title page; 
    b) a summary of the report; 
    c) a table of contents;
    d) the body of the report, suitably subdivided into sections; 
    e) a table comparing the estimated and actual costs, the items to be compared are: 
        the material bought, supplies, labour, computer costs, plus any other expenses that may have occurred;
    f) conclusions and recommendations; and 
    g) references.

The length of the report should typically be 5000 words or 25 pages of double-spaced typing, not including tables, graphs,
diagrams, comparison of estimated and actual costs, etc. It is to be noted by students that they normally have three weeks at
the beginning of the term for course changes and to decide whether to take the project or not. Also, they have to submit their
project report before the end of lectures.