import matplotlib.pyplot as plt
import pandas as pd
import sys

#length comparison

df = pd.read_csv(sys.argv[1], delimiter = ",")
xb = df.loc[df['mode'] == "burst"]['length(Byte)']
xp = df.loc[df['mode'] == "periodic"]['length(Byte)']
yb = df.loc[df['mode'] == "burst"]['avgLatency(ms)']
yp = df.loc[df['mode'] == "periodic"]['avgLatency(ms)']

x_avg = [10, 20, 30, 40, 50]
yb_avg = df.loc[df['mode'] == "burst"].groupby('length(Byte)')['avgLatency(ms)'].mean()
yp_avg = df.loc[df['mode'] == "periodic"].groupby('length(Byte)')['avgLatency(ms)'].mean()

print(yb_avg)
print(yp_avg)

plt.xlabel("Length(Byte)")
plt.ylabel("Average Latency (ms)")
plt.scatter(xb, yb, color='red', label="Burst Mode")
plt.scatter(xp, yp, color='blue', label="Periodic Mode")
plt.plot(x_avg, yb_avg, color='k', label="Burst Mode Average")
plt.plot(x_avg, yp_avg, color='c', label="Periodic Mode Average")
plt.legend()
plt.title("Average Latency Comparison of Busrt and Periodic Mode with parallelism", fontdict= {'fontsize': 10})
plt.xlim([0, 60])
plt.ylim([0, 10])
# plt.show()
plt.savefig('../report/UW_report/figures/averageLatencyDiffLength.png', bbox_inches='tight')

df = pd.read_csv(sys.argv[2], delimiter = ",")
xb = df.loc[df['mode'] == "burst"]['length(Byte)']
xp = df.loc[df['mode'] == "periodic"]['length(Byte)']
yb = df.loc[df['mode'] == "burst"]['avgLatency(ms)']
yp = df.loc[df['mode'] == "periodic"]['avgLatency(ms)']

x_avg = [10, 20, 30, 40, 50]
yb_avg = df.loc[df['mode'] == "burst"].groupby('length(Byte)')['avgLatency(ms)'].mean()
yp_avg = df.loc[df['mode'] == "periodic"].groupby('length(Byte)')['avgLatency(ms)'].mean()

print(yb_avg)
print(yp_avg)

plt.clf()
plt.xlabel("Length(Byte)")
plt.ylabel("Average Latency (ms)")
plt.scatter(xb, yb, color='red', label="Burst Mode")
plt.scatter(xp, yp, color='blue', label="Periodic Mode")
plt.plot(x_avg, yb_avg, color='k', label="Burst Mode Average")
plt.plot(x_avg, yp_avg, color='c', label="Periodic Mode Average")
plt.legend()
plt.title("Average Latency Comparison of Busrt and Periodic Mode with parallelism", fontdict= {'fontsize': 10})
plt.xlim([0, 60])
plt.ylim([45, 75])
# plt.show()
plt.savefig('../report/UW_report/figures/averageLatencyDiffLengthP2.png', bbox_inches='tight')

