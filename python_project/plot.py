import matplotlib.pyplot as plt
import pandas as pd
import sys

# mode comparison

df = pd.read_csv(sys.argv[1], delimiter = ",")
x = df['mode']
y = df['avgLatency(ms)']

df2 = pd.read_csv(sys.argv[2], delimiter = ",")
x2 = df2['mode']
y2 = df2['avgLatency(ms)']

burstLat = df.loc[df['mode'] == "burst"]['avgLatency(ms)'].mean()
periodLat = df.loc[df['mode'] == "periodic"]['avgLatency(ms)'].mean()

burstMaxLat = df.loc[df['mode'] == "burst"]['maxLatency(ms)'].mean()
periodMaxLat = df.loc[df['mode'] == "periodic"]['maxLatency(ms)'].mean()

burstMinLat = df.loc[df['mode'] == "burst"]['minLatency(ms)'].mean()
periodMinLat = df.loc[df['mode'] == "periodic"]['minLatency(ms)'].mean()

burstMinLat_m = df.loc[df['mode'] == "burst"]['minLatency(ms)'].min()
periodMinLat_m = df.loc[df['mode'] == "periodic"]['minLatency(ms)'].min()

print("Average Busrt Latency:", burstLat)
print("Average Periodic Latency:", periodLat)
print("Percentage Difference:", (burstLat - periodLat) / periodLat * 100)
print("----")
print("Average Max Busrt Latency:", burstMaxLat)
print("Average Max Periodic Latency:", periodMaxLat)
print("Percentage Difference:", (burstMaxLat - periodMaxLat) / periodMaxLat * 100)
print("Times:", burstMaxLat / periodMaxLat)
print("----")
print("Average Min Busrt Latency:", burstMinLat)
print("Average Min Periodic Latency:", periodMinLat)
print("Percentage Difference:", (burstMinLat - periodMinLat) / periodMinLat * 100)
print("Times:", burstMinLat / periodMinLat)
print("----")
print("Min Busrt Latency:", burstMinLat_m)
print("Min Periodic Latency:", periodMinLat_m)

print("=======================================")

# print("Average Busrt Latency:", df2.loc[df2['mode'] == "burst"]['avgLatency(ms)'].mean())
# print("Average Periodic Latency:", df2.loc[df2['mode'] == "periodic"]['avgLatency(ms)'].mean())

burstLat = df2.loc[df2['mode'] == "burst"]['avgLatency(ms)'].mean()
periodLat = df2.loc[df2['mode'] == "periodic"]['avgLatency(ms)'].mean()

burstMaxLat = df2.loc[df2['mode'] == "burst"]['maxLatency(ms)'].mean()
periodMaxLat = df2.loc[df2['mode'] == "periodic"]['maxLatency(ms)'].mean()

burstMinLat = df2.loc[df2['mode'] == "burst"]['minLatency(ms)'].mean()
periodMinLat = df2.loc[df2['mode'] == "periodic"]['minLatency(ms)'].mean()


print("Average Busrt Latency:", burstLat)
print("Average Periodic Latency:", periodLat)
print("Percentage Difference:", (burstLat - periodLat) / periodLat * 100)
print("----")
print("Average Max Busrt Latency:", burstMaxLat)
print("Average Max Periodic Latency:", periodMaxLat)
print("Percentage Difference:", (burstMaxLat - periodMaxLat) / periodMaxLat * 100)
print("Times:", burstMaxLat / periodMaxLat)
print("----")
print("Average Min Busrt Latency:", burstMinLat)
print("Average Min Periodic Latency:", periodMinLat)
print("Percentage Difference:", (burstMinLat - periodMinLat) / periodMinLat * 100)
print("Times:", burstMinLat / periodMinLat)

# plt.xlabel("Mode")
# plt.ylabel("Average Latency (ms)")
# plt.xlim([-1, 2])
# # plt.ylim([-2, 2])
# plt.title("Average Latency Comparison of Busrt and Periodic Mode with no parallelism", fontdict= {'fontsize': 10})
# plt.scatter(x,y)
# # plt.show()
# plt.savefig('../report/UW_report/figures/averageLatencyDiffMode.png', bbox_inches='tight')

# plt.clf()
# plt.xlabel("Mode")
# plt.ylabel("Average Latency (ms)")
# plt.xlim([-1, 2])
# # plt.ylim([-2, 2])
# plt.title("Average Latency Comparison of Busrt and Periodic Mode with parallelism", fontdict= {'fontsize': 10})
# plt.scatter(x2,y2)
# plt.savefig('../report/UW_report/figures/averageLatencyDiffModeP2.png', bbox_inches='tight')

