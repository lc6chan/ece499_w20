import matplotlib.pyplot as plt
import pandas as pd
import sys

df = pd.read_csv(sys.argv[1], delimiter = ",")
df2 = pd.read_csv(sys.argv[2], delimiter = ",")

# x = ['No Parallelism','With Parallelism']
x = df['mode']
y_avg = df['avgLatency(ms)']
x_avg = y_avg.count() * ['No Parallelism']
y_avg2 = df2['avgLatency(ms)']
x_avg2 = y_avg2.count() * ['With Parallelism']

print("======= Period")
print("== P1:")
print("Max, Min, Avg:", y_avg.max(), y_avg.min(), y_avg.mean())

print("== P2:")
print("Max, Min, Avg:", y_avg2.max(), y_avg2.min(), y_avg2.mean())

print("Percentage Difference (MAX):", (y_avg2.max()-y_avg.max()) / y_avg.max() * 100 , "Times:", y_avg2.max() / y_avg.max())
print("Percentage Difference (AVG):", (y_avg2.mean()-y_avg.mean()) / y_avg.mean() * 100, "Times:", y_avg2.mean() / y_avg.mean())
print("Percentage Difference (MIN):", (y_avg2.min()-y_avg.min()) / y_avg.min() * 100, "Times:", y_avg2.min() / y_avg.min())

plt.xlabel("Parallelism")
plt.ylabel("Average Latency (ms)")
plt.xlim([-1, 2])
plt.ylim([0, 80])
plt.title("Average Latency Comparison with and without parallelism", fontdict= {'fontsize': 10})
plt.scatter(x_avg, y_avg, color='c', label="No Parallelism")
plt.scatter(x_avg2, y_avg2, color='b', label="With Parallelism")
# plt.scatter(y_avg2)
# plt.show()
plt.savefig('../report/UW_report/figures/averageLatencyDiffParallel.png', bbox_inches='tight')
