import matplotlib.pyplot as plt
import pandas as pd
import sys

df = pd.read_csv(sys.argv[1], delimiter = ",")
df2 = pd.read_csv(sys.argv[2], delimiter = ",")

burst_size = [10, 20, 30, 40]
yb_avg = df.loc[df['mode'] == "burst"].groupby('burstSize')['avgLatency(ms)'].mean()
yb_avg2 = df2.loc[df2['mode'] == "burst"].groupby('burstSize')['avgLatency(ms)'].mean()

print("======= Burst Size")
print("== P1:")
print("Max, Min:", yb_avg.max(), yb_avg.min())
print("Percentage Change:", (yb_avg.max()-yb_avg.min()) / yb_avg.max() * 100)

print("== P2:")
print("Max, Min:", yb_avg2.max(), yb_avg2.min())
print("Percentage Change:", (yb_avg2.max()-yb_avg2.min()) / yb_avg2.max() * 100)

fig, axs = plt.subplots(2, sharex= True)
ax = fig.add_subplot(111, frameon=False)
fig.suptitle("Average Latency Comparison of Burst Size within Burst Mode", fontdict= {'fontsize': 10})

ax.set_xlabel("Burst Size")
ax.set_ylabel("Average Latency (ms)")
ax.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.grid(False)


axs[0].set_title("No Parallelism", fontdict= {'fontsize': 8})
axs[0].set_ylim([5,8])
axs[0].plot(burst_size, yb_avg, color='c', label="No Parallelism")
axs[1].set_title("With Parallelism", fontdict= {'fontsize': 8})
axs[1].set_ylim([40,80])
axs[1].plot(burst_size, yb_avg2, color='b', label="With Parallelism")
# plt.show()
plt.savefig('../report/UW_report/figures/averageLatencyBurstSize.png', bbox_inches='tight')


plt.clf()
burstinterval = [100, 200, 300, 400, 500]
yb_avg = df.loc[df['mode'] == "burst"].groupby('burstInterval(ms)')['avgLatency(ms)'].mean()
yb_avg2 = df2.loc[df2['mode'] == "burst"].groupby('burstInterval(ms)')['avgLatency(ms)'].mean()

print("======= Burst Interval")
print("== P1:")
print("Max, Min:", yb_avg.max(), yb_avg.min())
print("Percentage Change:", (yb_avg.max()-yb_avg.min()) / yb_avg.max() * 100)

print("== P2:")
print("Max, Min:", yb_avg2.max(), yb_avg2.min())
print("Percentage Change:", (yb_avg2.max()-yb_avg2.min()) / yb_avg2.max() * 100)

fig, axs = plt.subplots(2, sharex= True)
ax = fig.add_subplot(111, frameon=False)
fig.suptitle("Average Latency Comparison of Burst Interval within Burst Mode", fontdict= {'fontsize': 10})

ax.set_xlabel("Burst Interval (ms)")
ax.set_ylabel("Average Latency (ms)")
ax.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.grid(False)


axs[0].set_title("No Parallelism", fontdict= {'fontsize': 8})
axs[0].set_ylim([5,8])
axs[0].plot(burstinterval, yb_avg, color='c', label="No Parallelism")
axs[1].set_title("With Parallelism", fontdict= {'fontsize': 8})
axs[1].set_ylim([40,80])
axs[1].plot(burstinterval, yb_avg2, color='b', label="With Parallelism")
# plt.show()
plt.savefig('../report/UW_report/figures/averageLatencyBurstInterval.png', bbox_inches='tight')
