import socket, random, string, subprocess, time, sys
from datetime import datetime

def randomString(stringLength=10):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def generateDataSet(length = 10, size = 10):
    return [randomString(length)+'\n' for n in range(size)]

def generate_log_filename(suffix = ""):
    return "sender_log/flink_bm_" + suffix + "_" + ".log"

def sendBurstData(
    log_path = "",
    host = "localhost", port = 4444, 
    length = 10, interval = 0.3, burst_size = 10,
    duration = 60):

    logs = []
    start_time = time.time()

    with open(log_path, 'w') as output:
        output.write("length, duration, burst_size, burst_interval(ms): " +
            ', '.join(str(s) for s in [length, duration, burst_size, int(interval*1000)]) + '\n')
    
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        while (time.time() - start_time) < duration:

            data_set = generateDataSet(length = length, size = burst_size)

            interval_start_ts = time.time()

            for data in data_set:
                now = datetime.now()
                s.sendall(str.encode(data))
                logs.append((data[:-1], int(now.microsecond / 1000) ,time.time() * 1000))
            
            interval_end_ts = time.time()
            if interval > (interval_end_ts - interval_start_ts):
                time.sleep(interval - (interval_end_ts - interval_start_ts))
            else:
                time.sleep(interval)
        

    with open(log_path, 'a') as output:
        for log in logs:
            l = [str(ele) for ele in log]
            out = ','.join(l) + '\n'
            output.write(out)

    print("BUSRT MODE", "Length:", length, "Interval:", interval, "Burst Size:", burst_size, "DONE")

def sendPeriodicData(
    log_path = "",
    host = "localhost", port = 4444, 
    length = 10, period = 0.3,
    duration = 60):

    logs = []

    start_time = time.time()
    data_set = generateDataSet(length = length, size = int(duration/period))
    i = 0
    
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))

        while (time.time() - start_time) < duration:
            data = data_set[i]
            now = datetime.now()
            s.sendall(str.encode(data))
            logs.append((data[:-1], int(now.microsecond / 1000) ,time.time() * 1000))
            i = i+1
            time.sleep(period)
    successRate = round(len(logs)/(duration/period)*100, 2)

    with open(log_path, 'w') as output:
        output.writelines("length, duration, period(ms), sampleSent, successRate: " + 
            ', '.join(str(s) for s in [length, duration, int(period*1000), len(logs), successRate]) + '\n')

    with open(log_path, 'a') as output:
        for log in logs:
            l = [str(ele) for ele in log]
            out = ','.join(l) + '\n'
            output.write(out)

    print('PERIOD MODE', "Length:", length, "Period:", period, "Total Data Sent", len(logs))


DURATION = int(sys.argv[1])
MIN_LENGTH, MAX_LENGTH, LENGTH_STEP = [int(i) for i in sys.argv[2].split(',')]
MIN_BURST_INTEVAL, MAX_BURST_INTEVAL, BURST_INTEVAL_STEP = [int(i) for i in sys.argv[3].split(',')]
MIN_BURST_SIZE, MAX_BURST_SIZE, BURST_SIZE_STEP = [int(i) for i in sys.argv[4].split(',')]
MIN_PERIOD, MAX_PERIOD, PERIOD_STEP = [int(i) for i in sys.argv[5].split(',')]

# python3 benchmark.py 
#   {DURATION(s)}
#   {MIN_LENGTH, MAX_LENGTH, LENGTH_STEP} 
#   {MIN_BURST_INTEVAL(ms), MAX_BURST_INTEVAL(ms), BURST_INTEVAL_STEP(ms)}
#   {MIN_BURST_SIZE, MAX_BURST_SIZE, BURST_SIZE_STEP} 
#   {MIN_PERIOD(ms), MAX_PERIOD(ms), PERIOD_STEP(ms)}

# python3 benchmark.py 300 5,1000,5 10,500,10 1,50,1 10,500,10

# print(DURATION)
# print(MIN_LENGTH, MAX_LENGTH, LENGTH_STEP)
# print(MIN_BURST_INTEVAL, MAX_BURST_INTEVAL, BURST_INTEVAL_STEP)
# print(MIN_BURST_SIZE, MAX_BURST_SIZE, BURST_SIZE_STEP)
# print(MIN_PERIOD, MAX_PERIOD, PERIOD_STEP)


for length in range(MIN_LENGTH, MAX_LENGTH + LENGTH_STEP, LENGTH_STEP):
    for interval in range(MIN_BURST_INTEVAL, MAX_BURST_INTEVAL + BURST_INTEVAL_STEP, BURST_INTEVAL_STEP):
            for burst_size in range(MIN_BURST_SIZE, MAX_BURST_SIZE + BURST_SIZE_STEP, BURST_SIZE_STEP):
                t = "burst_" + str(length) +  "_interval_" + str(interval) + "_burstSize_" + str(burst_size)
                burst_file_name = generate_log_filename(suffix = t)
                
                sendBurstData(
                    log_path = burst_file_name,
                    length = length,
                    interval = interval/1000,
                    burst_size = burst_size,
                    duration = DURATION)
                time.sleep(2)
                # print(length, interval, burst_size)

print("Busrt data done")
time.sleep(5)

for length in range(MIN_LENGTH, MAX_LENGTH + LENGTH_STEP, LENGTH_STEP):
    for period in range(MIN_PERIOD, MAX_PERIOD + PERIOD_STEP, PERIOD_STEP):
        t = "burst_" + str(length) + "_period_" + str(period)
        periodic_file_name = generate_log_filename(suffix = t)

        sendPeriodicData(
            log_path = periodic_file_name,
            length = length,
            period = period/1000,
            duration = DURATION)
        time.sleep(2)
        # print(length, period)

print("Periodic data done")