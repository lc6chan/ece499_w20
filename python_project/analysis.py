import os, glob, sys
import numpy

LOG_FOLDER = sys.argv[1]
OUTPUT_SOURCE = sys.argv[2]
OUTPUT_PATH = sys.argv[3]

def calculate_latency(send_ts, rec_ts):

    if send_ts <= rec_ts:
        return rec_ts - send_ts
    else:
        return rec_ts + (1000 - send_ts)

def matchAndFindLatency(log_file, mode, 
    length, duration, 
    burst_size, burst_interval,
    period, sampleSent, successRate):

    inputs = {}
    final_result = ""
    latencies = []

    for line in log_file.readlines():
        # print(line)
        data_id, tsInMs, tsInFull = line.split(",")
        inputs[data_id] = (tsInMs, tsInFull)
    
        # latencies = [calculate_latency(int(inputs[in_key][0]), int(outputs[in_key][0])) for in_key in set(inputs.keys()).intersection(set(outputs.keys()))]
        for in_key in inputs.keys():
            if in_key in outputs.keys():
                latencies.append(calculate_latency(int(inputs[in_key][0]), int(outputs[in_key][0])))
    
    if len(latencies) == 0:
        error = mode + "," \
            + duration + "," \
            + length + "," \
            + burst_size + "," \
            + burst_interval + "," \
            + period + "," \
            + sampleSent + "," \
            + successRate
        print("Empty latencies" + error)
    else:
        final_result = mode + "," \
            + duration + "," \
            + length + "," \
            + "{0:.2f}".format(sum(latencies)/len(latencies)) + "," \
            + str(min(latencies)) + "," \
            + str(max(latencies)) + "," \
            + "{0:.3f}".format(numpy.std(latencies)) + "," \
            + burst_size + "," \
            + burst_interval + "," \
            + period + "," \
            + sampleSent + "," \
            + successRate + "\n"
    
    return final_result

outputs = {}
results = {}
final_result_csv = \
    "mode,duration(s),length(Byte), \
    avgLatency(ms),minLatency(ms),maxLatency(ms), \
    standardDeviation, \
    burstSize, burstInterval(ms), \
    period(ms), sampleSent, successRate\n"

with open(OUTPUT_SOURCE, 'r') as o:
    for line in  o.readlines():
        data_id, tsInMs, tsInFull = line.split(",")
        outputs[data_id] = (tsInMs, tsInFull)

burst_logs = [n for n in glob.glob(LOG_FOLDER+"flink_bm_burst_*interval*", recursive=False)]
# print(burst_logs)
for input_file in burst_logs:
    with open(input_file, 'r') as i:
        general_info = i.readline()
        # print(general_info)
        key = general_info.split(":")[1].replace(" ","").strip()
        length, duration, burst_size, burst_interval = key.split(",")
        period, sampleSent, successRate = ["-1", "-1", "-1"]

        result = matchAndFindLatency(
            log_file = i, mode = "burst", 
            length = length, duration = duration, 
            burst_size = burst_size, burst_interval = burst_interval,
            period = period, sampleSent = sampleSent, successRate = successRate)

        if result != "":
            results[key] = result
            print(results[key].strip())

for k in sorted(results.keys()):
    final_result_csv = final_result_csv + results[k]

results = {}
periodic_logs = [n for n in glob.glob(LOG_FOLDER+"flink_bm_burst_*period_*", recursive=False)]
# print(periodic_logs)
for input_file in periodic_logs:
    with open(input_file, 'r') as i:
        general_info = i.readline()
        key = general_info.split(":")[1].replace(" ","").strip()
        length, duration, period, sampleSent, successRate = key.split(",")
        burst_size, burst_interval = ["-1", "-1"]

        result = matchAndFindLatency(
            log_file = i, mode = "periodic", 
            length = length, duration = duration, 
            burst_size = burst_size, burst_interval = burst_interval,
            period = period, sampleSent = sampleSent, successRate = successRate)
        
        if result != "":
            results[key] = result
            print(results[key].strip())
        
for k in sorted(results.keys()):
    final_result_csv = final_result_csv + results[k]

with open(OUTPUT_PATH, 'w') as output_path:
    output_path.write(final_result_csv)

